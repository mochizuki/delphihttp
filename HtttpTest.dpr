program HtttpTest;

{$R *.dres}

uses
  Vcl.Forms,
  Main in 'Main.pas' {MainForm},
  HttpUnit in 'HttpUnit.pas';

{$R *.res}

begin
{$IFDEF DEBUG}
  ReportMemoryLeaksOnShutdown := true; // DebugHook<>0;
{$ENDIF}
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TMainForm, MainForm);
  Application.Run;
end.
