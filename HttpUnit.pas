unit HttpUnit;

interface

uses system.Classes, qworker, IdHTTP, IdSSLOpenSSL, IdIOHandlerSocket,
  IdIOHandlerStack, IdSSL, system.SysUtils, idcookiemanager, idheaderlist,
  IdGlobalProtocols, IdCompressorZLib;

type
  THttpMethod = (ePost, eGet);

  /// <summary>POST数据时的结构</summary>
  TPostStruct = class
  public
    url: string;
    params: TStringList;
    constructor Create; virtual;
    destructor Destroy; override;
  end;

  /// <summary>返回的数据结构</summary>
  TResponseStruct = class
    stream: TMemoryStream;
    datatype: String;
    Responseheader: TIdHeaderList;
    constructor Create; virtual;
    destructor Destroy; override;
  end;

  THttp = class
  private

    mOpenSSL: TIdSSLIOHandlerSocketOpenSSL;

    procedure doHttpGet(AJob: PQJob);
    procedure doHttpPost(AJob: PQJob);

    procedure doHttpGetStream(AJob: PQJob);

    procedure Init(http: TIdHTTP);

    /// <summary>对Response进行处理</summary>
    /// <param name="stream">待处理的数据流</param>
    /// <param name="pResponse">Response头的数据</param>
    /// <returns>Response的文本内容</returns>
    function ProcessResponse(pStream: TMemoryStream;
      pResponse: TIdHttpResponse): string;

    /// <summary>保存cookie信息</summary>
    /// <param name="CookieManager">CookieManager</param>
    /// <returns>cookie信息</returns>
    procedure SaveCookie(CookieManager: TidCookieManager);

    /// <summary>执行http请求，取得的数据以流的形式返回，压缩过的数据则已经解压缩</summary>
    /// <param name="pHttp">TIdHTTP对象</param>
    /// <param name="pUrl">地址</param>
    /// <param name="pMethod">请求方式</param>
    /// <param name="pParams">POST的参数</param>
    /// <param name="pOutStream">返回的数据流</param>
    // procedure ExcuteHttp(pHttp: TIdHTTP; pUrl: string; pMethod: THttpMethod;
    // pParams: TStringList; pOutStream: TMemoryStream);
  public
    constructor Create; virtual;
    destructor Destroy; override;

    procedure HttpGet(url: String);
    procedure HttpGetImage(url: String);
    procedure HttpPost(url: string; params: TStringList);

    procedure ClearCookie;

    class var gLoginCookie: TStringList;
    /// <summary>显示消息/日志</summary>
    class var SignalShowLog: integer;
    class var SignalRunning: integer;
    class var SignalStoped: integer;
    class var SignalData: integer;
  end;

implementation

{ THttp }

procedure THttp.ClearCookie;
begin
  gLoginCookie.Clear;
end;

constructor THttp.Create;
begin
  mOpenSSL := TIdSSLIOHandlerSocketOpenSSL.Create(nil);
  // gLoginCookie.Clear;
end;

destructor THttp.Destroy;
begin
  mOpenSSL.Free;
  inherited;
end;

procedure THttp.doHttpGet(AJob: PQJob);
var
  url, Response: string;
  lIdHTTP: TIdHTTP;
  stream: TMemoryStream;
begin
  workers.Signal(SignalRunning, nil);
  url := TQJobExtData(AJob.Data).AsString;
  lIdHTTP := TIdHTTP.Create(nil);
  stream := TMemoryStream.Create;
  try
    try
      Init(lIdHTTP);
      // ExcuteHttp(lIdHTTP,url,eGet,nil,stream);
      lIdHTTP.Get(url, stream);
      Response := self.ProcessResponse(stream, lIdHTTP.Response);
      SaveCookie(lIdHTTP.CookieManager);

      Response := lIdHTTP.Response.RawHeaders.CommaText + #13 + #10 + #13 + #10
        + Response;

      workers.Signal(SignalShowLog, TQJobExtData.Create(Response),
        jdfFreeAsObject);
    except
      on e: exception do
      begin
        // if e.Message='HTTP/1.1 302 Moved Temporarily' then
        // begin
        //
        // end else
        workers.Signal(SignalShowLog,
          TQJobExtData.Create(e.Message + #13 + #10 +
          lIdHTTP.Response.RawHeaders.CommaText), jdfFreeAsObject);
      end;
    end;
  finally
    lIdHTTP.Free;
    stream.Free;
    workers.Signal(SignalStoped, nil);
  end;
end;

procedure THttp.doHttpGetStream(AJob: PQJob);
var
  url: string;
  lIdHTTP: TIdHTTP;
  struct: TResponseStruct;
begin
  workers.Signal(SignalRunning, nil);
  url := TQJobExtData(AJob.Data).AsString;
  lIdHTTP := TIdHTTP.Create(nil);
  struct := TResponseStruct.Create;
  try
    try
      Init(lIdHTTP);
      lIdHTTP.Get(url, struct.stream);
      // ExcuteHttp(lIdHTTP,url,eGet,nil,struct.stream);

      struct.datatype := lIdHTTP.Response.ContentType;
      struct.Responseheader.Assign(lIdHTTP.Response.RawHeaders);

      SaveCookie(lIdHTTP.CookieManager);

      struct.stream.Position := 0;

      workers.Signal(SignalData, struct, jdfFreeAsObject);
    except
      on e: exception do
      begin
        workers.Signal(SignalShowLog, TQJobExtData.Create(e.Message),
          jdfFreeAsObject);
        struct.Free;
      end;
    end;
  finally
    lIdHTTP.Free;
    workers.Signal(SignalStoped, nil);
  end;

end;

procedure THttp.doHttpPost(AJob: PQJob);
var
  url, Response: string;
  param: TStringList;
  lIdHTTP: TIdHTTP;
  mstream: TMemoryStream;
begin
  workers.Signal(SignalRunning, nil);
  url := TPostStruct(AJob.Data).url;
  param := TPostStruct(AJob.Data).params;
  lIdHTTP := TIdHTTP.Create(nil);
  mstream := TMemoryStream.Create;
  try
    try
      Init(lIdHTTP);
      lIdHTTP.Post(url, param, mstream);
      // ExcuteHttp(lIdHTTP,url,ePost,param,mstream);
      Response := lIdHTTP.Response.RawHeaders.CommaText + #13 + #10 + #13 + #10
        + self.ProcessResponse(mstream, lIdHTTP.Response);

      SaveCookie(lIdHTTP.CookieManager);

      workers.Signal(SignalShowLog, TQJobExtData.Create(Response),
        jdfFreeAsObject);

    except
      on e: exception do
        workers.Signal(SignalShowLog, TQJobExtData.Create(e.Message),
          jdfFreeAsObject);
    end;
  finally
    lIdHTTP.Free;
    mstream.Free;
    workers.Signal(SignalStoped, nil);
  end;

end;

// procedure THttp.ExcuteHttp(pHttp: TIdHTTP; pUrl: string; pMethod: THttpMethod;
// pParams: TStringList; pOutStream: TMemoryStream);
// var
// sStream: TMemoryStream;
// begin
// sStream := TMemoryStream.Create;
// try
// case pMethod of
// ePost:
// pHttp.Post(pUrl, pParams, sStream);
// eGet:
// pHttp.Get(pUrl, sStream);
// end;
// sStream.Position := 0;
// if pHttp.Response.ContentEncoding = 'gzip' then
// pHttp.Compressor.DecompressGZipStream(sStream, pOutStream)
// else
// pOutStream.LoadFromStream(sStream);
// finally
// sStream.Free;
// end;
// end;

procedure THttp.SaveCookie(CookieManager: TidCookieManager);
var
  i: integer;
  tmp: TStringList;
  ct: string;
  j: integer;
begin
  tmp := TStringList.Create;
  try
    for i := 0 to CookieManager.CookieCollection.Count - 1 do
    begin

      ct := CookieManager.CookieCollection.cookies[i].CookieText;
      tmp.Delimiter := ';';
      tmp.StrictDelimiter := true;
      tmp.DelimitedText := ct;
      for j := 0 to tmp.Count - 1 do
      begin
        self.gLoginCookie.Values[tmp.Names[j]] := tmp.Values[tmp.Names[j]];
      end;
    end;
  finally
    tmp.Free;
  end;

end;

procedure THttp.HttpGet(url: String);
begin
  workers.Post(doHttpGet, TQJobExtData.Create(url), false, jdfFreeAsObject);
end;

procedure THttp.HttpGetImage(url: String);
begin
  workers.Post(doHttpGetStream, TQJobExtData.Create(url), false,
    jdfFreeAsObject);

end;

procedure THttp.HttpPost(url: string; params: TStringList);
var
  datas: TPostStruct;
begin
  datas := TPostStruct.Create;
  datas.url := url;
  datas.params.Assign(params);
  workers.Post(doHttpPost, datas, false, jdfFreeAsObject)
end;

procedure THttp.Init(http: TIdHTTP);
begin
  http.AllowCookies := true;
  http.HandleRedirects := true;
  http.IOHandler := self.mOpenSSL;
  http.ProtocolVersion := pv1_1;
  http.HTTPOptions := [hoForceEncodeParams, hoTreat302Like303];
  http.Compressor := TIdCompressorZLib.Create(http);
  http.Request.AcceptEncoding := 'gzip, deflate, sdch';
  http.Request.AcceptLanguage := 'zh-CN,zh;q=0.8';
  http.Request.Connection := 'keep-alive';
  http.Request.Accept :=
    'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8';
  // http.Request.UserAgent := 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.106 Safari/537.36';
  if self.gLoginCookie.Text <> '' then
  begin
    gLoginCookie.Delimiter := ';';
    gLoginCookie.StrictDelimiter := true;

    http.Request.CustomHeaders.Values['Cookie'] := gLoginCookie.DelimitedText;
    // http.Request.RawHeaders.Values['Cookie'] := mLoginCookie; //这一行不也可以
    // +';'+http.Request.RawHeaders.Values['Cookie'];
  end;

end;

function THttp.ProcessResponse(pStream: TMemoryStream;
  pResponse: TIdHttpResponse): string;
var
  sStream: TStringStream;
  scharset: string;
  p: integer;
begin
  scharset := pResponse.RawHeaders.Values['Content-Type'];

  p := pos('charset', scharset);
  if p <= 0 then
  begin
    scharset := '';
  end
  else
  begin
    p := pos('=', scharset, p);
    scharset := scharset.Substring(p, scharset.Length);
  end;

  scharset := uppercase(scharset);
  if { (scharset = '') or } (scharset = 'UTF-8') then
  begin
    sStream := TStringStream.Create('', TEncoding.UTF8);

  end
  else
  begin
    sStream := TStringStream.Create('', TEncoding.Default);
  end;
  try
    pStream.Position := 0;
    sStream.LoadFromStream(pStream);
    result := sStream.DataString;
  finally
    sStream.Free;
  end;
end;

procedure initData();
begin
  THttp.gLoginCookie := TStringList.Create;
  THttp.SignalShowLog := workers.RegisterSignal('showlog');
  THttp.SignalRunning := workers.RegisterSignal('Running');
  THttp.SignalStoped := workers.RegisterSignal('Stoped');
  THttp.SignalData := workers.RegisterSignal('Data');
end;

procedure finalData();
begin
  THttp.gLoginCookie.Free;
end;

{ TPostStruct }

constructor TPostStruct.Create;
begin
  params := TStringList.Create;

end;

destructor TPostStruct.Destroy;
begin
  params.Free;
  inherited;
end;

{ TResponseStruct }

constructor TResponseStruct.Create;
begin
  stream := TMemoryStream.Create;
  Responseheader := TIdHeaderList.Create(QuoteHTTP);

end;

destructor TResponseStruct.Destroy;
begin
  Responseheader.Free;
  stream.Free;
  inherited;
end;

initialization

initData();

finalization

finalData();

end.
