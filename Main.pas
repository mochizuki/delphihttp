unit Main;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Imaging.jpeg, Vcl.Imaging.pngimage,
  Vcl.Imaging.GIFImg, qworker, HttpUnit,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls,
  Vcl.ComCtrls;

type
  TMainForm = class(TForm)
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet3: TTabSheet;
    Label1: TLabel;
    Edit1: TEdit;
    Button1: TButton;
    ZhEdit: TEdit;
    PswEdit: TEdit;
    Label3: TLabel;
    Label4: TLabel;
    Button3: TButton;
    Edit5: TEdit;
    Label5: TLabel;
    Memo2: TMemo;
    Panel1: TPanel;
    Image2: TImage;
    NameFieldEdit: TEdit;
    PsdFieldEdit: TEdit;
    Label6: TLabel;
    RandomFieldEdit: TEdit;
    RandomCodeEdit: TEdit;
    Image3: TImage;
    Edit3: TEdit;
    Label7: TLabel;
    Button4: TButton;
    Button2: TButton;
    procedure Button1Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
    mHttp: THttp;

    procedure LoadAnimate;

    procedure ShowLog(AJob: PQJob);
    procedure ShowImage(AJob: PQJob);
    procedure ThreadRuning(AJob: PQJob);
    procedure ThreadStoped(AJob: PQJob);

    procedure ThreadA(AJob: PQJob);
    procedure ThreadB(AJob: PQJob);
  public
    { Public declarations }
  end;

var
  MainForm: TMainForm;

implementation

{$R *.dfm}

procedure TMainForm.Button1Click(Sender: TObject);
begin
  // Memo1.Lines.Text := GetHttp(Edit1.Text);
  mHttp.HttpGet(Edit1.Text);
end;

procedure TMainForm.Button2Click(Sender: TObject);
begin
  workers.Post(ThreadA, nil, false);
end;

procedure TMainForm.Button3Click(Sender: TObject);
var
  param: TStringList;
begin

  param := TStringList.Create;
  try
    param.Add(NameFieldEdit.Text + '=' + ZhEdit.Text);
    param.Add(PsdFieldEdit.Text + '=' + PswEdit.Text);
    param.Add(RandomFieldEdit.Text + '=' + RandomCodeEdit.Text);
    // Memo2.Text := Post(Edit5.Text, param);
    mHttp.HttpPost(Edit5.Text, param);
  finally
    param.Free;
  end;
end;

procedure TMainForm.Button4Click(Sender: TObject);
begin
  mHttp.HttpGetImage(Edit3.Text);

end;

procedure TMainForm.FormCreate(Sender: TObject);
begin
  mHttp := THttp.Create;
  LoadAnimate;

  workers.Wait(ShowLog, THttp.SignalShowLog, true);
  workers.Wait(ThreadRuning, THttp.SignalRunning, true);
  workers.Wait(ThreadStoped, THttp.SignalStoped, true);
  workers.Wait(ShowImage, THttp.SignalData, true);

end;

procedure TMainForm.FormDestroy(Sender: TObject);
begin
  mHttp.Free;
end;

procedure TMainForm.LoadAnimate;
var
  gif: TGIFImage;
  stream: TResourceStream;
begin
  gif := TGIFImage.Create;
  stream := TResourceStream.Create(HINSTANCE, 'GifImage_1', RT_RCDATA);
  try
    gif.LoadFromStream(stream);
    // gif.LoadFromFile(ExtractFilePath(Application.Exename) + 'run.gif');
    gif.AnimationSpeed := 100;
    gif.Animate := true;
    Image2.Picture.Graphic := gif;
  finally
    stream.Free;
    gif.Free;
  end;

end;

procedure TMainForm.ShowImage(AJob: PQJob);
var
  data: TResponseStruct;
  img: TGraphic;
  // ext: string;
begin
  img := nil;
  data := TResponseStruct(AJob.data);
  // ext := ExtractFileExt(Edit2.Text);
  Memo2.Lines.Add(data.datatype);
  Memo2.Lines.Add(data.Responseheader.Values['Set-Cookie']);
  if lowercase(data.datatype) = 'image/png' then
  begin
    img := TPngImage.Create;
  end
  else if lowercase(data.datatype) = 'image/jpeg' then
    img := TJpegImage.Create
  else if lowercase(data.datatype) = 'image/bmp' then
    img := TBitmap.Create
  else if lowercase(data.datatype) = 'image/gif' then
  begin
    img := TGIFImage.Create;
    TGIFImage(img).AnimationSpeed := 100;
    TGIFImage(img).Animate := true;
  end;

  if not assigned(img) then
    exit;
  try
    data.stream.Position := 0;
    img.LoadFromStream(data.stream);
    Image3.Picture.Graphic := img;
  finally
    img.Free;
  end;

end;

procedure TMainForm.ShowLog(AJob: PQJob);
var
  txt: string;
begin
  txt := TQJobExtData(AJob.data).AsString;
  Memo2.Text := THttp.gLoginCookie.DelimitedText + #13 + #10 + #13 + #10 + txt;
end;

procedure TMainForm.ThreadA(AJob: PQJob);
var
  i: int64;
begin
  i := 101;

  workers.Post(ThreadB, TQJobExtData.Create(i), true, jdfFreeAsObject);
end;

procedure TMainForm.ThreadB(AJob: PQJob);
var
  v: int64;
begin
  v := AJob.ExtData.AsInt64;
  Edit1.Text := inttostr(v);
end;

procedure TMainForm.ThreadRuning(AJob: PQJob);
begin
  Image2.Visible := true;
end;

procedure TMainForm.ThreadStoped(AJob: PQJob);
begin
  Image2.Visible := false;
end;

end.
