object MainForm: TMainForm
  Left = 0
  Top = 0
  Caption = 'MainForm'
  ClientHeight = 677
  ClientWidth = 882
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 882
    Height = 241
    ActivePage = TabSheet1
    Align = alTop
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = 'Get'#32593#39029
      object Label1: TLabel
        Left = 11
        Top = 19
        Width = 19
        Height = 13
        Caption = 'URL'
      end
      object Edit1: TEdit
        Left = 51
        Top = 16
        Width = 679
        Height = 21
        TabOrder = 0
        Text = 'https://www.baidu.com/'
      end
      object Button1: TButton
        Left = 51
        Top = 43
        Width = 75
        Height = 25
        Caption = 'Get'
        TabOrder = 1
        OnClick = Button1Click
      end
      object Button2: TButton
        Left = 200
        Top = 43
        Width = 145
        Height = 25
        Caption = 'qworker int64'#27979#35797
        TabOrder = 2
        OnClick = Button2Click
      end
    end
    object TabSheet3: TTabSheet
      Caption = #32858#33021#21147#30331#24405#27979#35797
      ImageIndex = 2
      object Label3: TLabel
        Left = 72
        Top = 43
        Width = 39
        Height = 13
        Caption = #24080' '#21495#65306
      end
      object Label4: TLabel
        Left = 72
        Top = 78
        Width = 39
        Height = 13
        Caption = #23494' '#30721#65306
      end
      object Label5: TLabel
        Left = 72
        Top = 6
        Width = 60
        Height = 13
        Caption = #35831#27714#22320#22336#65306
      end
      object Label6: TLabel
        Left = 72
        Top = 113
        Width = 48
        Height = 13
        Caption = #39564#35777#30721#65306
      end
      object Image3: TImage
        Left = 455
        Top = 113
        Width = 170
        Height = 53
      end
      object Label7: TLabel
        Left = 72
        Top = 153
        Width = 67
        Height = 13
        Caption = #39564#35777#30721'URL'#65306
      end
      object ZhEdit: TEdit
        Left = 264
        Top = 40
        Width = 185
        Height = 21
        TabOrder = 0
      end
      object PswEdit: TEdit
        Left = 264
        Top = 75
        Width = 185
        Height = 21
        PasswordChar = '*'
        TabOrder = 1
      end
      object Button3: TButton
        Left = 488
        Top = 38
        Width = 75
        Height = 25
        Caption = #30331#24405
        TabOrder = 2
        OnClick = Button3Click
      end
      object Edit5: TEdit
        Left = 136
        Top = 3
        Width = 313
        Height = 21
        TabOrder = 3
        Text = 'http://www.junengli.com/login/userLogin'
      end
      object NameFieldEdit: TEdit
        Left = 137
        Top = 40
        Width = 121
        Height = 21
        TabOrder = 4
        Text = 'username'
        TextHint = ' '#23383#27573#21517
      end
      object PsdFieldEdit: TEdit
        Left = 137
        Top = 75
        Width = 121
        Height = 21
        TabOrder = 5
        Text = 'password'
        TextHint = ' '#23383#27573#21517
      end
      object RandomFieldEdit: TEdit
        Left = 137
        Top = 110
        Width = 121
        Height = 21
        TabOrder = 6
        Text = 'vercode'
        TextHint = ' '#23383#27573#21517
      end
      object RandomCodeEdit: TEdit
        Left = 264
        Top = 110
        Width = 185
        Height = 21
        TabOrder = 7
      end
      object Edit3: TEdit
        Left = 136
        Top = 150
        Width = 232
        Height = 21
        TabOrder = 8
        Text = 
          'http://www.junengli.com/securityCodeImageAction?name=login&fontS' +
          'ize=13'
      end
      object Button4: TButton
        Left = 374
        Top = 148
        Width = 75
        Height = 25
        Caption = #21047#26032#39564#35777#30721
        TabOrder = 9
        OnClick = Button4Click
      end
    end
  end
  object Memo2: TMemo
    Left = 0
    Top = 281
    Width = 882
    Height = 396
    Align = alClient
    ScrollBars = ssBoth
    TabOrder = 1
  end
  object Panel1: TPanel
    Left = 0
    Top = 241
    Width = 882
    Height = 40
    Align = alTop
    Caption = 'Panel1'
    TabOrder = 2
    object Image2: TImage
      Left = 39
      Top = 2
      Width = 32
      Height = 32
      Visible = False
    end
  end
end
